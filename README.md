# Task 11 - Java - Movie Character API

Full CRUD REST API made with Spring / Hibernate. DB used is PostgreSQL.
Hosted at Heroku. 

###### Developers: Robin Burø. Hans Erling Eidsvold. Henrik Heiberg.

#### Documentation
Full documentation at: https://movie-character-api.herokuapp.com/swagger-ui/

Preview:

<img src="/src/main/resources/screenshots/Swagger.PNG"  width="600">


#### Postman

Postman API Collection with all available endpoints here:
https://documenter.getpostman.com/view/12289600/TVRedqoW