package no.noroff.MovieCharacterAPI.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RecordNotFoundException extends RuntimeException {

    private String exceptionMessage;
    private Long id;

    public RecordNotFoundException(String exceptionMessage, Long id){
        super(exceptionMessage + " - " + id);
        this.exceptionMessage = exceptionMessage;
        this.id = id;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public Long getId() {
        return id;
    }
}
