package no.noroff.MovieCharacterAPI.models.dto;

import no.noroff.MovieCharacterAPI.models.Movie;
import java.util.Set;

public class FranchiseAddOrUpdateDTO {


    private String name;
    private String description;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
