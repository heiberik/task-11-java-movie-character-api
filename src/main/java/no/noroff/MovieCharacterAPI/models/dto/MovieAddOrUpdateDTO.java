package no.noroff.MovieCharacterAPI.models.dto;

public class MovieAddOrUpdateDTO {

    private String title;
    private String[] genres;
    private String releaseYear;
    private String director;
    private String img;
    private String trailer;
    private Long franchiseId;

    public String getTitle() {
        return title;
    }

    public String[] getGenres() {
        return genres;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public String getImg() {
        return img;
    }

    public String getTrailer() {
        return trailer;
    }

    public Long getFranchiseId() {
        return franchiseId;
    }
}
