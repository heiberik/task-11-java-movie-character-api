package no.noroff.MovieCharacterAPI.models.dto;

public class CharacterAddOrUpdateDTO {

    private String name;

    private String alias;

    private String gender;

    private String img;

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }

    public String getGender() {
        return gender;
    }

    public String getImg() {
        return img;
    }
}
