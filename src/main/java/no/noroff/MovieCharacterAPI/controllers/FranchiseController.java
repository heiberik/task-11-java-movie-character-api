package no.noroff.MovieCharacterAPI.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Franchise;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.models.dto.FranchiseAddOrUpdateDTO;
import no.noroff.MovieCharacterAPI.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/franchises")
@Api(value = "Franchise", tags = { "Franchise" })
public class FranchiseController {

    @Autowired
    private FranchiseService franchiseService;

    // get all franchises
    @GetMapping
    @ApiOperation(value="Get all franchises", tags = { "Franchise" })
    public ResponseEntity<List<Franchise>> getAllFranchises(){

        List<Franchise> franchises = franchiseService.getAllFranchises();
        return new ResponseEntity<>(franchises, HttpStatus.OK);
    }

    // get a spesific franchise
    @GetMapping("/{id}")
    @ApiOperation(value="Get a specific franchise", tags = { "Franchise" })
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){

        Franchise franchise = franchiseService.getSpecificFranchise(id);
        return new ResponseEntity<>(franchise, HttpStatus.OK);
    }

    // get all movies for a specific franchise
    @GetMapping("/{id}/movies")
    @ApiOperation(value="Get all movies for a specific franchise", tags = { "Franchise" })
    public ResponseEntity<Set<Movie>> getAllMovies(@PathVariable Long id) {

        Set<Movie> movies = franchiseService.getAllMovies(id);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    // get all characters in a franchise
    @GetMapping("/{id}/characters")
    @ApiOperation(value="Get all characters in a franchise", tags = { "Franchise" })
    public ResponseEntity<Set<Character>> getAllCharactersInFranchise(@PathVariable Long id) {

        Set<Character> characters = franchiseService.getAllCharactersInFranchise(id);
        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    // add a franchise
    @PostMapping
    @ApiOperation(value="Add a franchise", tags = { "Franchise" })
    public ResponseEntity<Franchise> addFranchise(@Valid @RequestBody Franchise franchise){

        Franchise addedFranchise = franchiseService.addFranchise(franchise);
        return new ResponseEntity<>(addedFranchise, HttpStatus.OK);
    }

    // update a franchise
    @PutMapping("/{id}")
    @ApiOperation(value="Update a franchise", tags = { "Franchise" })
    public ResponseEntity<Franchise> updateFranchise(@RequestBody FranchiseAddOrUpdateDTO franchise,
                                                     @PathVariable Long id){
        Franchise updatedFranchise = franchiseService.updateFranchise(franchise, id);
        return new ResponseEntity<>(updatedFranchise, HttpStatus.OK);
    }

    // add movies to the franchise
    @PatchMapping("/{id}/movies")
    @ApiOperation(value="Add movies to a franchise", tags = { "Franchise" })
    public ResponseEntity<Franchise> addMovies(@PathVariable Long id, @Valid @RequestBody List<Long> movieIds) {

        Franchise updatedFranchiser = franchiseService.addMoviesToFranchise(id, movieIds);
        return new ResponseEntity<>(updatedFranchiser, HttpStatus.OK);
    }

    // delete a franchise
    @DeleteMapping("/{id}")
    @ApiOperation(value="Delete a franchise", tags = { "Franchise" })
    public ResponseEntity<String> deleteFranchise(@PathVariable Long id) {
        
        franchiseService.deleteFranchise(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
