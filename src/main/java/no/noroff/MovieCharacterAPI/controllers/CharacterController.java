package no.noroff.MovieCharacterAPI.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import no.noroff.MovieCharacterAPI.exceptions.RecordNotFoundException;
import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.models.dto.CharacterAddOrUpdateDTO;
import no.noroff.MovieCharacterAPI.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/characters")
@Api(value = "Character", tags = { "Character" })
public class CharacterController {

    @Autowired
    private CharacterService characterService;

    // Get all characters
    @GetMapping
    @ApiOperation(value="Get all characters", tags = { "Character" })
    public ResponseEntity<List<Character>> getAllCharacters() {

        List<Character> characters = characterService.getAllCharacters();
        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    // Get a specific character
    @GetMapping("/{id}")
    @ApiOperation(value="Get a specific character", tags = { "Character" })
    public ResponseEntity<Character> getCharacter(@PathVariable Long id) {

        Character character = characterService.getCharacter(id);
        return new ResponseEntity<>(character, HttpStatus.OK);
    }

    // Get all movies by a character
    @GetMapping("/{id}/movies")
    @ApiOperation(value="Get all movies by a character", tags = { "Character" })
    public ResponseEntity<Set<Movie>> getAllMoviesByCharacter(@PathVariable Long id) {

        Set<Movie> movies = characterService.getAllMoviesByCharacter(id);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    // Add a new character
    @PostMapping
    @ApiOperation(value="Add a new character", tags = { "Character" })
    public ResponseEntity<Character> addCharacter(@Valid @RequestBody CharacterAddOrUpdateDTO dto) {

        Character addedCharacter = characterService.addCharacter(dto);
        return new ResponseEntity<>(addedCharacter, HttpStatus.OK);
    }

    // update character
    @PutMapping("/{id}")
    @ApiOperation(value="Update character", tags = { "Character" })
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id,
                                                     @Valid @RequestBody CharacterAddOrUpdateDTO dto) {

        Character updatedCharacter = characterService.updateCharacter(id, dto);
        return new ResponseEntity<>(updatedCharacter, HttpStatus.OK);
    }

    // Add movies to a specific character
    @PatchMapping("/{id}/movies")
    @ApiOperation(value="Add movies to a specific character", tags = { "Character" })
    public ResponseEntity<Character> addMovies(@PathVariable Long id,
                                               @Valid @RequestBody List<Long> movieIds) {

        Character updatedCharacter = characterService.addMoviesToCharacter(id, movieIds);
        return new ResponseEntity<>(updatedCharacter, HttpStatus.OK);
    }

    // delete a character
    @DeleteMapping("/{id}")
    @ApiOperation(value="Delete a character", tags = { "Character" })
    public ResponseEntity<?> deleteCharacter(@PathVariable Long id) {

        characterService.deleteCharacter(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
