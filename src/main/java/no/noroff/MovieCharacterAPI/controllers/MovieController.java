package no.noroff.MovieCharacterAPI.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.models.dto.MovieAddOrUpdateDTO;
import no.noroff.MovieCharacterAPI.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/movies")
@Api(value = "Movie", tags = { "Movie" })
public class MovieController {

    @Autowired
    private MovieService movieService;

    // Get all movies from database
    @GetMapping
    @ApiOperation(value="Get all movies", tags = { "Movie" })
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieService.getAllMovies();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    // Get a movie by id from the database
    @GetMapping("/{id}")
    @ApiOperation(value="Get a specific movie by id", tags = { "Movie" })
    public ResponseEntity<Movie> getMovie(@PathVariable Long id) {
        Movie movie = movieService.getMovie(id);
        return new ResponseEntity<>(movie, HttpStatus.OK);
    }

    // Get all characters in a specific movie
    @GetMapping("/{id}/characters")
    @ApiOperation(value="Get all characters in a specific movie", tags = { "Movie" })
    public ResponseEntity<Set<Character>> getAllCharactersInMovie(@PathVariable Long id) {
        Set<Character> characters = movieService.getAllCharactersInMovie(id);
        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    // Add a new movie to the database
    @PostMapping
    @ApiOperation(value="Add a new movie", tags = { "Movie" })
    public ResponseEntity<Movie> addMovie(@RequestBody MovieAddOrUpdateDTO movie) {
        Movie addedMovie = movieService.addMovie(movie);
        return new ResponseEntity<>(addedMovie, HttpStatus.OK);
    }

    // Update a movie in the database
    @PutMapping("/{id}")
    @ApiOperation(value="Update a movie", tags = { "Movie" })
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @Valid @RequestBody MovieAddOrUpdateDTO dto) {
        Movie updatedMovie = movieService.updateMovie(id, dto);
        return new ResponseEntity<>(updatedMovie, HttpStatus.OK);
    }

    // Add characters to a movie
    @PatchMapping("/{id}/characters")
    @ApiOperation(value="Add characters to a movie", tags = { "Movie" })
    public ResponseEntity<Movie> addCharactersToMovie(@PathVariable Long id, @Valid @RequestBody List<Long> characterIds) {
        Movie updatedMovie = movieService.addCharacterToMovie(id, characterIds);

        return new ResponseEntity<>(updatedMovie, HttpStatus.OK);
    }

    // Delete a movie in the database
    @DeleteMapping("/{id}")
    @ApiOperation(value="Delete a movie", tags = { "Movie" })
    public ResponseEntity<String> deleteMovie(@PathVariable Long id) {
        movieService.deleteMovie(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
