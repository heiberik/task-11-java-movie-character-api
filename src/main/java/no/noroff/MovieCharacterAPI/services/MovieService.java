package no.noroff.MovieCharacterAPI.services;

import no.noroff.MovieCharacterAPI.exceptions.RecordNotFoundException;
import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Franchise;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.models.dto.MovieAddOrUpdateDTO;
import no.noroff.MovieCharacterAPI.repositories.CharacterRepository;
import no.noroff.MovieCharacterAPI.repositories.FranchiseRepository;
import no.noroff.MovieCharacterAPI.repositories.MovieRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private ModelMapper modelMapper;

    // Get all movies from database
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    // Get a movie by id from the database
    public Movie getMovie(Long id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isEmpty()) {
            throw new RecordNotFoundException("No Movie exists with id", id);
        } else {
            return movie.get();
        }
    }

    // Add a new movie to the database
    public Movie addMovie(MovieAddOrUpdateDTO dto) {

        Movie movie = modelMapper.map(dto, Movie.class);

        if (dto.getFranchiseId() != null) {
            Franchise franchise = franchiseRepository.findById(dto.getFranchiseId())
                    .orElseThrow(() -> new RecordNotFoundException("No franchise exists with id", dto.getFranchiseId()));

            movie.setFranchise(franchise);
        }

        return movieRepository.save(movie);
    }

    // Update a movie in the database
    public Movie updateMovie(Long id, MovieAddOrUpdateDTO movieDTO) {

        return movieRepository.findById(id)
                .map(movie -> {

                    movie.setTitle(movieDTO.getTitle());
                    movie.setGenres(movieDTO.getGenres());
                    movie.setReleaseYear(movieDTO.getReleaseYear());
                    movie.setDirector(movieDTO.getDirector());
                    movie.setImg(movieDTO.getImg());
                    movie.setTrailer(movieDTO.getTrailer());

                    if (movieDTO.getFranchiseId() != null) {
                        Franchise franchise = franchiseRepository.findById(movieDTO.getFranchiseId())
                                .orElseThrow(() -> new RecordNotFoundException("No franchise exists with id", movieDTO.getFranchiseId()));
                        movie.setFranchise(franchise);
                    }

                    return movieRepository.save(movie);
                })
                .orElseThrow(() -> new RecordNotFoundException("Movie does not exist with id", id));
    }

    // Delete a movie in the database
    public void deleteMovie(Long id) {

        Movie movie = movieRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No movie exists with id", id));

        for (Character character : movie.getCharacters()) {
            movie.removeCharacter(character);
        }

        movieRepository.delete(movie);
    }

    // Add characters to a movie
    public Movie addCharacterToMovie(Long movieId, List<Long> characterIds) {
        Movie movie = movieRepository.findById(movieId)
                .orElseThrow(() -> new RecordNotFoundException("Movie does not exist with id", movieId));

        movie.getCharacters().addAll(characterRepository.findAllById(characterIds));

        movie = movieRepository.save(movie);

        return movie;
    }

    // Get all characters in a specific movie
    public Set<Character> getAllCharactersInMovie(Long id) {
        if (movieRepository.existsById(id)) {
            if (movieRepository.findById(id).isPresent()) {
                Movie movie = movieRepository.findById(id).get();
                return movie.getCharacters();
            } else {
                throw new RecordNotFoundException("No movie exists with id", id);
            }
        }
        throw new RecordNotFoundException("No movie exists with id", id);
    }
}
