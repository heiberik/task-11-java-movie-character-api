package no.noroff.MovieCharacterAPI.services;

import no.noroff.MovieCharacterAPI.exceptions.RecordNotFoundException;
import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.models.dto.CharacterAddOrUpdateDTO;
import no.noroff.MovieCharacterAPI.repositories.CharacterRepository;
import no.noroff.MovieCharacterAPI.repositories.MovieRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @author Hans Erling Eidsvold
 *
 * This service handles all business logic of Character
 */
@Service
public class CharacterService {

    private final String CHARACTER_NOT_FOUND = "No character exists with id";

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Finds all characters in the repository
     * @return List of type Character
     */
    public List<Character> getAllCharacters() {

        return characterRepository.findAll();
    }

    /**
     * Finds a specific character in the repository
     * @param characterId
     * @return Character
     */
    public Character getCharacter(Long characterId) {

        return characterRepository.findById(characterId)
                .orElseThrow(() -> new RecordNotFoundException(CHARACTER_NOT_FOUND, characterId));
    }

    /**
     * Gets all movies in which the character is featured in
     * @param characterId
     * @return Set of Movies
     */
    public Set<Movie> getAllMoviesByCharacter(Long characterId) {

        return characterRepository.findById(characterId).map(Character::getMovies)
                .orElseThrow(() -> new RecordNotFoundException(CHARACTER_NOT_FOUND, characterId));
    }

    /**
     * Adds a new Character to the repository
     * @param characterDTO data values of new Character
     * @return the new Character
     */
    public Character addCharacter(CharacterAddOrUpdateDTO characterDTO) {

        Character character = modelMapper.map(characterDTO, Character.class);

        character = characterRepository.save(character);

        return character;
    }

    /**
     * Finds an existing Character and updates given data fields
     * @param characterId
     * @param characterDTO data values to be updated
     * @return the updated Character
     */
    public Character updateCharacter(Long characterId, CharacterAddOrUpdateDTO characterDTO) {

        return characterRepository.findById(characterId)
                .map(character -> {
                    character.setName(characterDTO.getName());
                    character.setAlias(characterDTO.getAlias());
                    character.setGender(characterDTO.getGender());
                    character.setImg(characterDTO.getImg());
                    return characterRepository.save(character);
                })
                .orElseThrow(() -> new RecordNotFoundException(CHARACTER_NOT_FOUND, characterId));
    }

    /**
     * Adds the given character in one or multiple movies
     * @param characterId
     * @param movieIds of movies we want to add to character
     * @return the updated character
     */
    public Character addMoviesToCharacter(Long characterId, List<Long> movieIds) {

        Character character = characterRepository.findById(characterId)
                .orElseThrow(() -> new RecordNotFoundException(CHARACTER_NOT_FOUND, characterId));

        character.getMovies().addAll(movieRepository.findAllById(movieIds));

        character = characterRepository.save(character);

        return character;
    }

    /**
     * Deletes a specified Character from the repository
     * @param characterId
     */
    public void deleteCharacter(Long characterId) {

        if (!characterRepository.existsById(characterId)) {
            throw new RecordNotFoundException(CHARACTER_NOT_FOUND, characterId);
        }

        characterRepository.deleteById(characterId);
    }
}
