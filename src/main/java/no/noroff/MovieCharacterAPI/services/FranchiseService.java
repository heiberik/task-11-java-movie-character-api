package no.noroff.MovieCharacterAPI.services;

import no.noroff.MovieCharacterAPI.exceptions.RecordNotFoundException;
import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Franchise;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.models.dto.FranchiseAddOrUpdateDTO;
import no.noroff.MovieCharacterAPI.repositories.FranchiseRepository;
import no.noroff.MovieCharacterAPI.repositories.MovieRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class FranchiseService {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MovieRepository movieRepository;


    public List<Franchise> getAllFranchises(){
        return franchiseRepository.findAll();
    }

    public Franchise getSpecificFranchise(Long id){
        Optional<Franchise> franchise = franchiseRepository.findById(id);
        if (franchise.isEmpty()){
            throw new RecordNotFoundException("No franchise exists with id", id);
        }
        else {
            return franchise.get();
        }
    }

    public Franchise addFranchise(Franchise dto){
        return franchiseRepository.save(dto);
    }

    public Franchise updateFranchise(FranchiseAddOrUpdateDTO dto, Long id){

        return franchiseRepository.findById(id)
                .map(franchise -> {
                    franchise.setName(dto.getName());
                    franchise.setDescription(dto.getDescription());
                    return franchiseRepository.save(franchise);
                })
                .orElseThrow(() -> new RecordNotFoundException("No franchise exists with id", id));
    }

    public Franchise addMoviesToFranchise(Long franchiseId, List<Long> movieIds) {

        Franchise franchise = franchiseRepository.findById(franchiseId)
                .orElseThrow(() -> new RecordNotFoundException("No franchise exists with id", franchiseId));

        List<Movie> movies = movieRepository.findAllById(movieIds);

        for (Movie movie : movies) {
            movie.setFranchise(franchise);
        }

        franchise = franchiseRepository.save(franchise);

        return franchise;
    }

    public Set<Movie> getAllMovies(Long id){
        if (franchiseRepository.existsById(id)) {
            Optional<Franchise> franchise = franchiseRepository.findById(id);
            return franchise.get().getMovies();
        } else {
            throw new RecordNotFoundException("No franchise exists with id", id);
        }
    }

    public Set<Character> getAllCharactersInFranchise(Long id){
        if (franchiseRepository.existsById(id)) {
            Set<Character> list = new HashSet<>();
            for (Movie m : franchiseRepository.findById(id).get().getMovies()){
                list.addAll(m.getCharacters());
            }
            return list;
        } else {
            throw new RecordNotFoundException("No franchise exists with id", id);
        }
    }

    public void deleteFranchise(Long id){

        Franchise franchise = franchiseRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("No franchise exists with id", id));

        for (Movie movie : franchise.getMovies()) {
            movie.setFranchise(null);
        }


        franchiseRepository.delete(franchise);
    }
}
